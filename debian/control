Source: input-remapper
Maintainer: Stephen Kitt <skitt@debian.org>
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Section: misc
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               gir1.2-glib-2.0,
               gir1.2-gtk-3.0,
               gir1.2-gtksource-4,
               python3-all,
               python3-evdev,
               python3-gi,
               python3-psutil,
               python3-pydantic,
               python3-pydbus,
               python3-setuptools
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/input-remapper
Vcs-Git: https://salsa.debian.org/python-team/packages/input-remapper.git
Homepage: https://github.com/sezanzeb/input-remapper
Rules-Requires-Root: no

Package: input-remapper
Architecture: all
Depends: input-remapper-gtk,
         ${misc:Depends}
Description: Input device button mapping tool (metapackage)
 input-remapper allows users to map buttons on all input devices
 (keyboards, mice, gamepads...) in X11 and Wayland. It also supports
 combined buttons and programmable macros.
 .
 input-remapper includes a UI to configure button mappings, per
 device, and configuration to automatically apply button mappings at
 boot and on device connection.

Package: input-remapper-daemon
Architecture: all
Depends: python3-inputremapper,
         ${misc:Depends},
         ${python3:Depends}
Description: Input device button mapping tool (daemon)
 input-remapper allows users to map buttons on all input devices
 (keyboards, mice, gamepads...) in X11 and Wayland. It also supports
 combined buttons and programmable macros.
 .
 This package contains the service and udev configuration to
 automatically apply button mappings at boot and on device
 connection.

Package: input-remapper-gtk
Architecture: all
Depends: input-remapper-daemon,
         python3-inputremapper,
         gir1.2-glib-2.0,
         gir1.2-gtk-3.0,
         gir1.2-gtksource-4,
         ${misc:Depends},
         ${python3:Depends}
Description: Input device button mapping tool (GUI)
 input-remapper allows users to map buttons on all input devices
 (keyboards, mice, gamepads...) in X11 and Wayland. It also supports
 combined buttons and programmable macros.
 .
 This package contains the UI to configure the mappings.

Package: python3-inputremapper
Architecture: all
Section: python
Depends: ${misc:Depends},
         ${python3:Depends}
Recommends: gir1.2-glib-2.0,
            gir1.2-gtk-3.0,
            gir1.2-gtksource-4,
Description: Input device button mapping tool (Python module)
 input-remapper allows users to map buttons on all input devices
 (keyboards, mice, gamepads...) in X11 and Wayland. It also supports
 combined buttons and programmable macros.
 .
 This package contains the Python module implementing the remapper.
 The Glib and Gtk dependencies are only required to use the
 inputremapper.gui module.
